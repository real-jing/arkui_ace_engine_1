/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Chip, ChipSize } from '@ohos.arkui.advanced.Chip'


interface ChipGroupTheme {
  itemStyle: ChipItemStyle;
  chipGroupSpace: ChipGroupSpaceOptions;
  leftPadding: number;
  rightPadding: number;
}

const noop = (selectedIndexes: Array<number>) => {
}
const colorStops: Array<[string, number]> = [['rgba(0, 0, 0, 1)', 0], ['rgba(0, 0, 0, 0)', 1]]
const defaultTheme: ChipGroupTheme = {
  itemStyle: {
    size: ChipSize.NORMAL,
    backgroundColor: $r('sys.color.ohos_id_color_button_normal'),
    fontColor: $r('sys.color.ohos_id_color_text_primary'),
    selectedFontColor: $r('sys.color.ohos_id_color_text_primary_contrary'),
    selectedBackgroundColor: $r('sys.color.ohos_id_color_emphasize'),
  },
  chipGroupSpace: { itemSpace: 8, startSpace: 16, endSpace: 16 },
  leftPadding: 16,
  rightPadding: 16,
}

const iconGroupSuffixTheme: IconGroupSuffixTheme = {
  backgroundColor: $r('sys.color.ohos_id_color_button_normal'),
  borderRadius: $r('sys.float.ohos_id_corner_radius_tips_instant_tip'),
  smallIconSize: 16,
  normalIconSize: 24,
  smallBackgroundSize: 28,
  normalBackgroundSize: 36,
  marginLeft: 8,
  marginRight: 16,
  fillColor: $r('sys.color.ohos_id_color_primary')
}

interface IconOptions {
  src: ResourceStr;
  size?: SizeOptions;
}


interface LabelOptions {
  text: string;
}

export interface ChipGroupItemOptions {
  prefixIcon?: IconOptions;
  label: LabelOptions;
  suffixIcon?: IconOptions;
  allowClose?: boolean;
}

export interface ChipItemStyle {
  size?: ChipSize | SizeOptions;
  backgroundColor?: ResourceColor;
  fontColor?: ResourceColor;
  selectedFontColor?: ResourceColor;
  selectedBackgroundColor?: ResourceColor;
}

interface ChipGroupSpaceOptions {
  itemSpace?: number | string;
  startSpace?: Length;
  endSpace?: Length;
}


export interface IconItemOptions {
  icon: IconOptions,
  action: Callback<void>
}

interface IconGroupSuffixTheme {
  smallIconSize: number;
  normalIconSize: number;
  backgroundColor: ResourceColor;
  smallBackgroundSize: number;
  normalBackgroundSize: number;
  borderRadius: Dimension;
  marginLeft: number;
  marginRight: number;
  fillColor: ResourceColor;
}

function parseDimension<T>(
  uiContext: UIContext,
  value: Dimension | Length | undefined,
  isValid: Callback<string, boolean>,
  defaultValue: T
): T {
  if (value === void (0) || value === null) {
    return defaultValue;
  }
  const resourceManager = uiContext.getHostContext()?.resourceManager;
  if (typeof value === "object") {
    let temp: Resource = value as Resource;
    if (temp.type === 10002) {
      if (resourceManager.getNumber(temp.id) >= 0) {
        return value as T;
      }
    } else if (temp.type === 10003) {
      if (isValidDimensionString(resourceManager.getStringSync(temp.id))) {
        return value as T;
      }
    }
  } else if (typeof value === "number") {
    if (value >= 0) {
      return value as T;
    }
  } else if (typeof value === "string") {
    if (isValid(value)) {
      return value as T;
    }
  }
  return defaultValue;
}

function isValidString(dimension: string, regex: RegExp): boolean {
  const matches = dimension.match(regex);
  if (!matches || matches.length < 3) {
    return false;
  }
  const value = Number.parseFloat(matches[1]);
  return value >= 0;
}

function isValidDimensionString(dimension: string): boolean {
  return isValidString(dimension, new RegExp("(-?\\d+(?:\\.\\d+)?)_?(fp|vp|px|lpx|%)?$", "i"));
}

function isValidDimensionNoPercentageString(dimension: string): boolean {
  return isValidString(dimension, new RegExp("(-?\\d+(?:\\.\\d+)?)_?(fp|vp|px|lpx)?$", "i"));
}

@Component
export struct IconGroupSuffix {
  @Consume chipSize: ChipSize | SizeOptions;
  @Prop items: Array<IconItemOptions> = [];

  private getBackgroundSize(): number {
    if (this.chipSize === ChipSize.SMALL) {
      return iconGroupSuffixTheme.smallBackgroundSize;
    } else {
      return iconGroupSuffixTheme.normalBackgroundSize;
    }
  }

  private getIconSize(val: Length): Length {
    let value: Length
    if (this.chipSize === ChipSize.SMALL) {
      value = parseDimension(this.getUIContext(), val, isValidDimensionString, iconGroupSuffixTheme.smallIconSize);
    } else {
      value = parseDimension(this.getUIContext(), val, isValidDimensionString, iconGroupSuffixTheme.normalIconSize);
    }
    return value;
  }

  build() {
    Row() {
      ForEach(this.items, (suffixItem: IconItemOptions) => {
        Button() {
          Image(suffixItem.icon.src)
            .fillColor(iconGroupSuffixTheme.fillColor)
            .size({
              width: this.getIconSize(suffixItem.icon?.size?.width),
              height: this.getIconSize(suffixItem.icon?.size?.height)
            })
            .focusable(true)
        }
        .size({
          width: this.getBackgroundSize(),
          height: this.getBackgroundSize()
        })
        .backgroundColor(iconGroupSuffixTheme.backgroundColor)
        .borderRadius(iconGroupSuffixTheme.borderRadius)
        .onClick(() => {
          suffixItem.action();
        })
        .borderRadius(iconGroupSuffixTheme.borderRadius)
      })
    }
  }
}


@Component
export struct ChipGroup {
  @Prop items: ChipGroupItemOptions[] = [];
  @Prop @Watch('itemStyleOnChange') itemStyle: ChipItemStyle = defaultTheme.itemStyle;
  @Provide chipSize: ChipSize | SizeOptions = defaultTheme.itemStyle.size;
  @Prop selectedIndexes: Array<number> = [0];
  @Prop multiple: boolean = false;
  @Prop chipGroupSpace: ChipGroupSpaceOptions = defaultTheme.chipGroupSpace;
  @BuilderParam suffix?: Callback<void>;
  public onChange: Callback<Array<number>> = noop;
  private scroller: Scroller = new Scroller();
  @State isReachEnd: boolean = this.scroller.isAtEnd();
  private chipCount: number = this.items.length;

  itemStyleOnChange() {
    this.chipSize = this.getChipSize();
  }

  aboutToAppear() {
    this.itemStyleOnChange();
  }

  private getChipSize(): ChipSize | SizeOptions {
    if (this.itemStyle && this.itemStyle.size) {
      return this.itemStyle.size
    }
    return defaultTheme.itemStyle.size;
  }

  private getFontColor(): ResourceColor {
    if (this.itemStyle && this.itemStyle.fontColor) {
      return this.itemStyle.fontColor
    }
    return defaultTheme.itemStyle.fontColor;
  }

  private getSelectedFontColor(): ResourceColor {
    if (this.itemStyle && this.itemStyle.selectedFontColor) {
      return this.itemStyle.selectedFontColor
    }
    return defaultTheme.itemStyle.selectedFontColor;
  }

  private getBackgroundColor(): ResourceColor {
    if (this.itemStyle && this.itemStyle.backgroundColor) {
      return this.itemStyle.backgroundColor
    }
    return defaultTheme.itemStyle.backgroundColor;
  }

  private getSelectedBackgroundColor(): ResourceColor {
    if (this.itemStyle && this.itemStyle.selectedBackgroundColor) {
      return this.itemStyle.selectedBackgroundColor
    }
    return defaultTheme.itemStyle.selectedBackgroundColor;
  }

  private getItemStyle(): ChipItemStyle {
    return this.itemStyle ?? defaultTheme.itemStyle;
  }

  private getSelectedIndexes(): Array<number> {
    let temp: number[] = [];
    temp = (this.selectedIndexes ?? [0]).filter(
      (element, index, array) => {
        return (element >= 0 && element % 1 == 0 && element <= this.chipCount);
      });
    return temp;
  }

  private isMultiple(): boolean {
    return this.multiple ?? false;
  }

  private getChipGroupItemSpace(): string | number {
    return parseDimension(
      this.getUIContext(),
      this.chipGroupSpace.itemSpace,
      isValidDimensionNoPercentageString,
      defaultTheme.chipGroupSpace.itemSpace
    );
  }

  private getChipGroupStartSpace() {
    return parseDimension(
      this.getUIContext(),
      this.chipGroupSpace.startSpace,
      isValidDimensionNoPercentageString,
      defaultTheme.chipGroupSpace.startSpace
    );
  }

  private getChipGroupEndSpace() {
    return parseDimension(
      this.getUIContext(),
      this.chipGroupSpace.endSpace,
      isValidDimensionNoPercentageString,
      defaultTheme.chipGroupSpace.endSpace
    );
  }

  private getOnChange(): (selectedIndexes: Array<number>) => void {
    return this.onChange ?? noop;
  }

  private isSelected(itemIndex: number): boolean {
    if (!this.isMultiple()) {
      return itemIndex == this.getSelectedIndexes()[0];
    } else {
      return this.getSelectedIndexes().some((element, index, array) => {
        return (element == itemIndex);
      })
    }
  }

  build() {

    Row() {
      Stack() {
        Scroll(this.scroller) {
          Row({ space: this.getChipGroupItemSpace() }) {
            ForEach(this.items || [], (chipItem: ChipGroupItemOptions, index) => {
              if (chipItem) {
                Chip({
                  prefixIcon: {
                    src: chipItem.prefixIcon?.src ?? "",
                    size: chipItem.prefixIcon?.size ?? undefined,
                  },
                  label: {
                    text: chipItem?.label?.text ?? " ",
                    fontColor: this.getFontColor(),
                    activatedFontColor: this.getSelectedFontColor(),
                  },
                  suffixIcon: {
                    src: chipItem.suffixIcon?.src ?? "",
                    size: chipItem.suffixIcon?.size ?? undefined,
                  },
                  allowClose: chipItem.allowClose ?? false,
                  enabled: true,
                  activated: this.isSelected(index),
                  backgroundColor: this.getBackgroundColor(),
                  size: this.getChipSize(),
                  activatedBackgroundColor: this.getSelectedBackgroundColor(),
                  onClicked: () => {
                    if (this.isSelected(index)) {
                      this.selectedIndexes.splice(this.selectedIndexes.indexOf(index), 1);
                    } else {
                      if (!this.isMultiple()) {
                        this.selectedIndexes = [];
                      }
                      this.selectedIndexes.push(index);
                    }
                    this.getOnChange()(this.getSelectedIndexes());
                  }
                })
              }
            });
          }
          .padding({ left: this.getChipGroupStartSpace(),
            right: this.getChipGroupEndSpace() })
          .constraintSize({ minWidth: '100%' })
        }
        .scrollable(ScrollDirection.Horizontal)
        .scrollBar(BarState.Off)
        .align(Alignment.Start)
        .width('100%')
        .onScroll(() => {
          this.isReachEnd = this.scroller.isAtEnd();
        })

        if (this.suffix && !this.isReachEnd) {
          Stack()
            .width(iconGroupSuffixTheme.normalBackgroundSize)
            .height(this.getChipSize() === ChipSize.SMALL ? iconGroupSuffixTheme.smallBackgroundSize : iconGroupSuffixTheme.normalBackgroundSize)
            .linearGradient({ angle: 90, colors: colorStops })
            .blendMode(BlendMode.DST_IN, BlendApplyType.OFFSCREEN)
            .hitTestBehavior(HitTestMode.None)
        }
      }
      .layoutWeight(1)
      .blendMode(BlendMode.SRC_OVER, BlendApplyType.OFFSCREEN)
      .alignContent(Alignment.End)

      if (this.suffix) {
        Row() {
          this.suffix();
        }.padding({ left: iconGroupSuffixTheme.marginLeft, right: iconGroupSuffixTheme.marginRight, })
      }
    }
    .align(Alignment.End)
    .width("100%")
    .height(64)
  }
}